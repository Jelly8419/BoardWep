﻿using BoardWepApp.Models;
using MySql.Data.MySqlClient;

namespace BoardWepApp
{
    public class DBService
    {
       public string ConnectionString { get; set; }

        public DBService(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        private MySqlConnection GetCnnection()
        {
            return new MySqlConnection(ConnectionString);
        }


        //회원리스트 가져오기
        public List<UserInfo> GetData()
        {
            List<UserInfo> list = new List<UserInfo>();
            string SQL = "SELECT* FROM UserInfo ";
            
            using (MySqlConnection connection = GetCnnection())
            {
                connection.Open();
                MySqlCommand cmd = new MySqlCommand(SQL, connection);

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new UserInfo()
                        {
                            id = reader["id"].ToString(),
                            name = reader["name"].ToString(),
                            password = reader["password"].ToString(),
                            address = reader["address"].ToString(),
                            email = reader["email"].ToString(),  
                            phone = Convert.ToInt32(reader["phone"])
                        });//end Add
                    }//end While
                }
                connection.Close();
                Console.WriteLine(list);
                return list;
            }
        }

        //회원정보 가져오기
        public UserInfo GetUserInfo(string id)
        {
            string SQL = $"select * from UserInfo where id= '{id}'";
            using (MySqlConnection connection = GetCnnection())
            {
                connection.Open();
                MySqlCommand cmd = new MySqlCommand(SQL, connection);
                var reader= cmd.ExecuteReader();
                return new UserInfo()
                {
                    id = reader["id"].ToString(),
                    name = reader["name"].ToString(),
                    password = reader["password"].ToString(),
                    address = reader["address"].ToString(),
                    email = reader["email"].ToString(),
                    phone = Convert.ToInt32(reader["phone"])
                };
            }
        }

        //아이디 중복체크 
        public int IdCheck(string id)
        {
            string SQL = "select Count(*) from UserInfo where id='"+id+"'";
            int count = 0;
            using (MySqlConnection connection = GetCnnection())
            {
                connection.Open();
                MySqlCommand cmd = new MySqlCommand(SQL, connection);

                count = Convert.ToInt32(cmd.ExecuteScalar());
                connection.Close();
                return count;
            }
        }

        //회원가입 정보 등록

        public void UserRegist(UserInfo userInfo)
        {
            var id=userInfo.id;
            var name=userInfo.name;
            var password=userInfo.password;
            var address=userInfo.address; 
            var email=userInfo.email;
            var phone=userInfo.phone;
            
            string SQL = $"insert into UserInfo(id,name,password,address,email,phone) values('{id}','{name}','{password}','{address}','{email}',{phone}) ";
            using (MySqlConnection connection = GetCnnection())
            {
                connection.Open();
                MySqlCommand cmd = new MySqlCommand(SQL, connection);
                cmd.ExecuteNonQuery();

                
                connection.Close();
            }
        }
       
        //로그인 시도 아이디,비밀번호 체크
        public string LoginTry(UserInfo userInfo)
        {
            var id = userInfo.id;
            var password=userInfo.password;

            string SQL = $"select*from UserInfo where id ='{id}'";
            string result = "";
            using(MySqlConnection connection = GetCnnection())
            {
                connection.Open();
                MySqlCommand cmd= new MySqlCommand(SQL, connection);
                var reader= cmd.ExecuteReader();

                while (reader.Read())
                {
                    if (reader["id"].ToString() == null)
                    {
                        result = "존재하지 않는 아이디입니다";

                    }
                    else if (reader["id"].ToString() != null && reader["password"].ToString() != password)
                    {
                        result = "비밀번호가 틀렸습니다";
                    }
                    else
                    {
                        result = "Success";
                    }
                }
                connection.Close();
                return result;
                

            }
        }
    }
    
}
