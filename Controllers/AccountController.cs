﻿using BoardWepApp.Models;
using Microsoft.AspNetCore.Mvc;

namespace BoardWepApp.Controllers
{
    public class AccountController : Controller
    {

        //회원가입 View
        public IActionResult SignUp()
        {
            return View();
        }

        //회원 정보수정 View
        public IActionResult ModifyInfo(string id)
        {
            DBService context = HttpContext.RequestServices.GetService(typeof(DBService)) as DBService;
            UserInfo user = context.GetUserInfo(id);
            return View(user);
        }

        //로그인 View
        public IActionResult Login()
        {
            return View();
        }


        //아이디 중복체크
        public int IdCheck(UserInfo userInfo)
        {

            DBService context = HttpContext.RequestServices.GetService(typeof(DBService)) as DBService;
            int count = context.IdCheck(userInfo.id);
            
            return count;
        }
        //회원가입클릭시 회원정보 등록
        public void UserRegist(UserInfo userInfo)
        {
            DBService context = HttpContext.RequestServices.GetService(typeof(DBService)) as DBService;
            context.UserRegist(userInfo);

            //로그인 창으로이동
            Response.Redirect(String.Format("Login"));

        }

        public string LoginTry(UserInfo userInfo)
        {
           
            DBService context = HttpContext.RequestServices.GetService(typeof(DBService)) as DBService;
            string result = context.LoginTry(userInfo);


            if (result.Equals("Success"))
            {   
                Response.Redirect(String.Format("/Board"));
                return "";
            }
            else return result;
        }

    }
}
